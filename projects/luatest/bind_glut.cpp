#include "stdafx.h"
#include <exception>
#include "lua.hpp"

#include "cpgf/gtestutil.h"
#include "cpgf/gmetadefine.h"
#include "cpgf/scriptbind/gscriptrunner.h"
#include "cpgf/scriptbind/gscriptbindutil.h"
#include "cpgf/gscopedinterface.h"
#include "cpgf/scriptbind/gluarunner.h"

#include "cpgf/gcompiler.h"
#include "cpgf/gmetadefine.h"

#if defined(_WIN32)
#include <windows.h>
#endif

#ifdef G_OS_APPLE
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

//#include "cpgf/metadata/opengl/gmetadata_opengl.h"
//#include "cpgf/metadata/opengl/gmetadata_openglu.h"
#include "cpgf/metadata/opengl/gmetadata_openglut.h"


using namespace cpgf;
using namespace std;


void registerOpenGLUT(GMetaClass * metaClass)
{
	GDefineMetaClass<void> define = GDefineMetaClass<void>::fromMetaClass(metaClass);

	buildMetaData_open_glut_constants(define);
	buildMetaData_open_glut_functions(define);
	buildMetaData_open_glut_helpers(define);
}