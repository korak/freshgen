// luatest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <exception>
#include "lua.hpp"

#include "cpgf/gtestutil.h"
#include "cpgf/gmetadefine.h"
#include "cpgf/scriptbind/gscriptrunner.h"
#include "cpgf/scriptbind/gscriptbindutil.h"
#include "cpgf/gscopedinterface.h"
#include "cpgf/scriptbind/gluarunner.h"

#include "utils.h"

namespace cpgf {
	struct IMetaService;
	struct IScriptObject;
} // namespace cpgf

class ScriptHelperImplement;

class ScriptHelper
{
public:
	ScriptHelper(int argc, char * argv[]);
	ScriptHelper(const char* filename);
	~ScriptHelper();

	bool execute();
	bool execureString(const char* script);

	cpgf::IMetaService * borrowService() const;
	cpgf::IScriptObject * borrowScriptObject() const;

private:
	cpgf::GScopedPointer<ScriptHelperImplement> implement;
};


#include <string.h>
#include <iostream>

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable:4996)
#endif


using namespace cpgf;
using namespace std;

enum ScriptLanguage {
	slLua
};

ScriptLanguage getScriptLanguageFromFileName(const char * fileName, bool useV8)
{
	return slLua;
}


GScriptRunner * createScriptRunnerFromScriptLanguage(ScriptLanguage lang, IMetaService * service)
{
	return createLuaScriptRunner(service);
}


const char * getLanguageText(ScriptLanguage lang)
{
	return "Lua";
}


void intializeScriptEngine(ScriptLanguage lang)
{
}


void finalizeScriptEngine(ScriptLanguage lang)
{

}



class ScriptHelperImplement
{
public:
	ScriptHelperImplement(int argc, char * argv[]);
	ScriptHelperImplement(const char* filename);
	~ScriptHelperImplement();

	bool execute();
	bool execureString(const char* script);

	IMetaService * borrowService() const {
		return this->service.get();
	}

	IScriptObject * borrowScriptObject() const {
		return this->scriptObject.get();
	}

private:
	void parseCommandLine(int argc, char * argv[]);

private:
	const char * fileName;
	ScriptLanguage scriptLanguage;
	GScopedPointer<GScriptRunner> runner;
	GScopedInterface<IMetaService> service;
	GScopedInterface<IScriptObject> scriptObject;
};

void die(const char * message)
{
	if(message != NULL)  {
		cerr << message << endl;
	}
	exit(1);
}

ScriptHelperImplement::ScriptHelperImplement(const char* filename)
{
	bool useV8 = false;
	this->fileName = filename;

	if(this->fileName == NULL) {
		die("Please specify a file name in the command line.");
	}

	FILE * file = fopen(this->fileName, "rb");
	if(file == NULL) {
		cerr << "File doesn't exist " << this->fileName << endl;
		die(NULL);
	}
	fclose(file);

	this->scriptLanguage = getScriptLanguageFromFileName(this->fileName, useV8);

	cout << "Running " << getLanguageText(this->scriptLanguage) << " script." << endl;
	cout << "File: " << this->fileName << endl;
	cout << endl;

	intializeScriptEngine(this->scriptLanguage);

	this->service.reset(createDefaultMetaService());
	this->runner.reset(createScriptRunnerFromScriptLanguage(this->scriptLanguage, this->service.get()));
	this->scriptObject.reset(runner->getScripeObject());

	this->scriptObject->bindCoreService("cpgf", NULL);
}

ScriptHelperImplement::ScriptHelperImplement(int argc, char * argv[])
	: fileName(NULL), scriptLanguage(slLua)
{
	this->parseCommandLine(argc, argv);

	cout << "Running " << getLanguageText(this->scriptLanguage) << " script." << endl;
	cout << "File: " << this->fileName << endl;
	cout << endl;

	intializeScriptEngine(this->scriptLanguage);

	this->service.reset(createDefaultMetaService());
	this->runner.reset(createScriptRunnerFromScriptLanguage(this->scriptLanguage, this->service.get()));
	this->scriptObject.reset(runner->getScripeObject());

	this->scriptObject->bindCoreService("cpgf", NULL);
}

ScriptHelperImplement::~ScriptHelperImplement()
{
	finalizeScriptEngine(this->scriptLanguage);
}

bool ScriptHelperImplement::execute()
{
	bool success = this->runner->executeFile(this->fileName);
	if(! success) {
		cerr << "Failed to execute file " << this->fileName << endl;
	}
	return success;
}


bool ScriptHelperImplement::execureString(const char* script) {
	bool success = true;
	this->runner->executeString(script);
	if (!success) {
		cerr << "Failed to execute file " << this->fileName << endl;
	}
	return success;
}

void ScriptHelperImplement::parseCommandLine(int argc, char * argv[])
{
	bool useV8 = false;
	this->fileName = NULL;

	for(int i = 1; i < argc; ++i) {
		const char * arg = argv[i];
		if(arg[0] == '-') {
			if(strcmp(arg, "-v8") == 0) {
				useV8 = true;
			}
		}
		else {
			if(this->fileName != NULL) {
				die("Only one file can be specified.");
			}
			this->fileName = arg;
		}
	}
	if(this->fileName == NULL) {
		die("Please specify a file name in the command line.");
	}

	FILE * file = fopen(this->fileName, "rb");
	if(file == NULL) {
		cerr << "File doesn't exist " << this->fileName << endl;
		die(NULL);
	}
	fclose(file);

	this->scriptLanguage = getScriptLanguageFromFileName(this->fileName, useV8);
}


ScriptHelper::ScriptHelper(int argc, char * argv[])
	: implement(new ScriptHelperImplement(argc, argv))
{
}

ScriptHelper::ScriptHelper(const char* filename)
	: implement(new ScriptHelperImplement(filename))
{
}

ScriptHelper::~ScriptHelper()
{
}

bool ScriptHelper::execute()
{
	return this->implement->execute();
}

bool ScriptHelper::execureString(const char* script) {
	return this->implement->execureString(script);
}

IMetaService * ScriptHelper::borrowService() const
{
	return this->implement->borrowService();
}

IScriptObject * ScriptHelper::borrowScriptObject() const
{
	return this->implement->borrowScriptObject();
}



#include "cpgf/gcompiler.h"
#include "cpgf/gmetadefine.h"

#if defined(_WIN32)
#include <windows.h>
#endif

#ifdef G_OS_APPLE
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
 #include <GL/glut.h>
#endif

//#include "cpgf/metadata/opengl/gmetadata_opengl.h"
//#include "cpgf/metadata/opengl/gmetadata_openglu.h"
//#include "cpgf/metadata/opengl/gmetadata_openglut.h"

using namespace cpgf;

//
//void registerOpenGL(GMetaClass * metaClass)
//{
//	GDefineMetaClass<void> define = GDefineMetaClass<void>::fromMetaClass(metaClass);
//
//	buildMetaData_open_gl(define);
//}
//
//void registerOpenGLU(GMetaClass * metaClass)
//{
//	GDefineMetaClass<void> define = GDefineMetaClass<void>::fromMetaClass(metaClass);
//
//	buildMetaData_open_glu(define);
//}


void registerOpenGLUT(GMetaClass * metaClass);

void exitDemo()
{
	exit(0);
}

void cpppring(const char* text) {
	std::cout << "LUA: " << text << std::endl;
}



int _tmain(int argc, _TCHAR* argv[])
{
	GDefineMetaNamespace define = GDefineMetaNamespace::declare("gl");

	registerOpenGLUT(define.getMetaClass());
	
	GDefineMetaGlobal()
		._method("exitDemo", &exitDemo)
		._method("cpppring", &cpppring);

	std::string lua_script;
	bool res = read_file("start.lua", lua_script);
	//bool res = read_file_to_string("start.lua", lua_script);

	ScriptHelper scriptHelper("luatest.lua");

	GScopedInterface<IMetaMethod> method_exitDemo(static_cast<IMetaMethod *>(metaItemToInterface(getGlobalMetaClass()->getMethod("exitDemo"))));
	GScopedInterface<IMetaMethod> method_cpppring(static_cast<IMetaMethod *>(metaItemToInterface(getGlobalMetaClass()->getMethod("cpppring"))));

	GScopedInterface<IMetaClass> glMetaClass(static_cast<IMetaClass *>(metaItemToInterface(define.getMetaClass())));
	scriptHelper.borrowScriptObject()->bindClass("gl", glMetaClass.get());
	
	scriptHelper.borrowScriptObject()->bindMethod("exitDemo", NULL, method_exitDemo.get());
	scriptHelper.borrowScriptObject()->bindMethod("cppprint", NULL, method_cpppring.get());

	//scriptHelper.execute();

	scriptHelper.execureString(lua_script.c_str());

	return 0;
}

