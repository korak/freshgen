#include "stdafx.h"



#include <opencv2/imgproc/imgproc.hpp>  // Gaussian Blur
#include <opencv2/core/core.hpp>        // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/highgui/highgui.hpp>  // OpenCV window I/O
//#include <opencv2/ml/ml.hpp>

#include "shared_lib/log_client.h"

using namespace cv;
using namespace std;

/// Global variables
int offset_x = 50;
int offset_y = 50;
int frame_width = -1;
int frame_height = -1;

#include "graphics_helpers.h"


/// Global variables
Mat src, src_gray, dst;

int maxCorners = 23;
int maxTrackbar = 100;

RNG rng(12345);
char* source_window = "Image";

char* windows[] = {"ch0", "ch1", "ch2"};

#include "./mongoose/mongoose.h"

bool is_calibrating = false;

// url, query string
typedef std::function<void(const char*, const char*)> request_handler_t;
typedef std::map<std::string, request_handler_t> req_handlers_t;
req_handlers_t req_handlers;


static int begin_request_handler(struct mg_connection *conn) {
	const struct mg_request_info *request_info = mg_get_request_info(conn);
	
	const char* uri = request_info->uri;
	const char* query_string = request_info->query_string;

	auto res = req_handlers.find(uri);
	if (res != req_handlers.end()) {
		res->second(uri, query_string);
		return 1;
	}

	return 0;

	//if (!request_info->query_string) 

	char content[100];

	// Prepare the message we're going to send
	int content_length = sprintf_s(content, sizeof(content),
		"Hello from mongoose! Remote port: %d",
		request_info->remote_port);

	// Send HTTP reply to the client
	mg_printf(conn,
		"HTTP/1.1 200 OK\r\n"
		"Content-Type: text/plain\r\n"
		"Content-Length: %d\r\n"        // Always set Content-Length
		"\r\n"
		"%s",
		content_length, content);

	// Returning non-zero tells mongoose that our function has replied to
	// the client, and mongoose should not send client any more data.
	return 1;
};


std::vector<Point2f> calibration_points;


int main( int argc, char** argv )
{
	bool debug_mode = false;
	cvStartWindowThread();

	req_handlers["/api/calibrate/stop/"] = [=](const char* uri, const char* query_string) {
		is_calibrating = false;
		destroyWindow(source_window);
	};

	req_handlers["/api/calibrate/start/"] = [=](const char* uri, const char* query_string) {
		is_calibrating = true;
		calibration_points.clear();
	};

	req_handlers["/api/swith/debugmode/"] = [&](const char* uri, const char* query_string) {
		debug_mode = !debug_mode;
	};

	//mg_callbacks web_callbacks;// = {0};
	struct mg_callbacks web_callbacks = {0};
	web_callbacks.begin_request = &begin_request_handler;

	logger::enable(true);
	logger::init("laser dot detector", 60009);

	VideoCapture captRefrnc(1);

	if ( !captRefrnc.isOpened()) {
		cout  << "Could not open reference " << 0 << endl;
		return -1;
	}

	double alpha = 0.5; 
	double beta = ( 1.0 - alpha );
	
	/// Create Window
	//namedWindow( source_window, CV_WINDOW_AUTOSIZE );

	//FILE* log = 0;
	//fwrite(log, "Started...");

	Size refS = Size((int) captRefrnc.get(CV_CAP_PROP_FRAME_WIDTH),
					 (int) captRefrnc.get(CV_CAP_PROP_FRAME_HEIGHT));

	frame_width = refS.width;
	frame_height = refS.height;

// hue
	Mat	hmin ( Scalar(245)  );
	Mat	hmax ( Scalar(255) );
// saturation
	Mat	smin ( Scalar(150) );
	Mat	smax ( Scalar(255) );
// value
	Mat	vmin ( Scalar(200) );
	Mat	vmax ( Scalar(260) );

	Mat hsv_image;
	Mat channels[30];
	Mat temp;

	Mat perspectiveTm;
	Mat perspectiveCam;

	Mat laser_img;

	//cvNamedWindow("main1",CV_WINDOW_NORMAL);
	//cvNamedWindow("main2",CV_WINDOW_AUTOSIZE | CV_GUI_NORMAL);
	//int value_hmin = 127;
	//cvCreateTrackbar( "hmin", "main1", &value_hmin, 255,  NULL);//OK tested
	//char* nameb1 = "button1";
	//char* nameb2 = "button2";
	//cvCreateButton(nameb1,callbackButton,nameb1,CV_CHECKBOX,1);

	const char *options[] = {
		"document_root", "./www",
		"listening_ports", "8080", 
		NULL
	};

	mg_context * contex = mg_start(&web_callbacks, NULL, options);

	for(;;) {
		int safe_zonex = offset_x/2;
		int safe_zoney = offset_y/2;

		captRefrnc >> src;
		
		//threshold( src, dst, 200, 500, THRESH_BINARY );
		cvtColor( src, hsv_image, CV_BGR2HSV );
		
		//1
		split(hsv_image, channels);
		//split the video frame into color channels
		//split(src, channels); // #2 - red

		//inRange(channels[0], hmin, hmax, channels[0]);
		inRange(channels[1], smin, smax, channels[1]);
		inRange(channels[2], vmin, vmax, channels[2]);

		//merge(channels, 3, hsv_image);
		//cvtColor( hsv_image, dst, CV_HSV2BGR );

		//threshold(  channels[2], temp, 225, 500, THRESH_BINARY );

		//add(channels[0], channels[1], laser_img);
		//add(channels[2], laser_img, laser_img);
		//add(channels[2], channels[2], laser_img);
		//multiply(channels[0], channels[1], laser_img);
		
		//add(channels[0], channels[1], laser_img);

		subtract(channels[2], channels[1], laser_img);
		//deflate

		//cvtColor( channels[2], temp, CV_BGR2HSV );
		
		//cvtColor( channels[2], hsv_image, CV_BGR2HSV );

		//cvtColor( dst, dst, CV_BGR2GRAY );
		//cvtColor( src, src_gray, CV_BGR2GRAY );
		//cvtColor( src, src_gray, CV_BGR2Luv );
		//cvConvertImage(src, src_gray, CV_BGR2Luv );

		//CV_BGR2Luv     =50,
		//CV_RGB2Luv     =51,
		//CV_BGR2HLS     =52,
		//CV_RGB2HLS     =53,

		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		//findContours( dst, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
		//findContours( temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

		findContours( laser_img, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );

		 //findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
		
		//addWeighted( src, alpha, src_gray, beta, 0.0, dst);
		Mat copy;
		copy = src.clone();

		points_t detected;

		int idx = 0;
		for( ; idx >= 0 && !hierarchy.empty(); idx = hierarchy[idx][0] )
		//if (!hierarchy.empty())
		{
			Scalar color( rand()&255, rand()&255, rand()&255 );
			if (contours.empty()) break;

			if (contours[idx].empty()) break;
			//Point p(0,0);
			//for (size_t i = 0; i < contours[idx].size();++i) {
			//	p +=  contours[idx][i];
			//}
			//p.x = (p.x*1.0f) / contours[idx].size();
			//p.y = (p.y*1.0f) / contours[idx].size();

			Mat points(contours[idx]);
			Point2f center;
			float r;
			minEnclosingCircle(points, center, r);
			
			//drawContours( copy, contours, idx, color, CV_FILLED, 8, hierarchy );
			//int r = 4;
			if (r < 2) continue;
			r = 8;
			//circle( src, center, r, Scalar(0, 255,0), -1, 8, 0 );
			detected.push_back(center);

			if (is_calibrating) {
				
				if (calibration_points.empty())
					calibration_points.push_back(center);
				else {
					Point2f p = calibration_points.back();
					float length = (p.x - center.x)*(p.x - center.x) + (p.y - center.y)*(p.y - center.y);
					if (length > 10) {
						calibration_points.push_back(center);
					}
					else {
						int iiii = 5;
					}
				}
			}

			if (is_calibrating && calibration_points.size() == 4) {
				points_t p = GetDstPoints();

				const Point2f dst[4] = {
					Point2f(p[0].x, p[0].y),
					Point2f(p[1].x, p[1].y),
					Point2f(p[2].x, p[2].y),
					Point2f(p[3].x, p[3].y)
				};
				const Point2f src[4] = {
					Point2f(calibration_points[0].x, calibration_points[0].y),
					Point2f(calibration_points[1].x, calibration_points[1].y),
					Point2f(calibration_points[2].x, calibration_points[2].y),
					Point2f(calibration_points[3].x, calibration_points[3].y)
				};
				is_calibrating = false;
				perspectiveTm = getPerspectiveTransform( src, dst );
				//= Mat.get
			}



			msg << "point(" << (int)(center.x*10) << "," << (int)(center.y*10) << ");";
			printf("%i, %i\n", (int)center.x, (int)center.y);
			//center
			//������� ����� ������.
		}

		if (debug_mode) {
		//imshow( source_window, dst);
		//imshow( source_window, src);
		//imshow( source_window, src);
		//imshow( source_window, hsv_image);
		//imshow( source_window, channels[1]);

			for (size_t i = 1; i < 3; ++i) {			
				imshow( windows[i], channels[i]);
			}
			imshow( "hsv", hsv_image);
			
			imshow( "laser", laser_img);

			//if (perspectiveTm.) 
			{
				warpPerspective(src, perspectiveCam, perspectiveTm, refS, INTER_LINEAR, BORDER_CONSTANT, 0);
				imshow( "Camera", perspectiveCam);
			}
		}

		if (is_calibrating) {
			{
				points_t p = GetDstPoints();
				for (size_t i = 0; i < p.size(); ++i) {
					gr::Circle(src, p[i]);
				}
			}

			{
				//detected.push_back(center);
				for (size_t i = 0; i < detected.size(); ++i) {
					gr::Circle(src, detected[i], 8, Scalar(0, 255,0));
				}
			}

			imshow( source_window, src);
		}
		//laser_img
		//normalize( source_window, src_gray, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
  
		
		if (cvWaitKey(1) == 27) break;
	}

  mg_stop(contex);
  return(0);
}
