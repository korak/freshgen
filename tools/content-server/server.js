var fs = require('fs'),
    sys = require('sys'),
    http = require('http');

var express = require('express');
var app = express();

var CONTENT_DIR = __dirname + '\\public\\';

console.log('root dir is', __dirname);


app.configure(function(){
	// app.use(express.logger('dev'));
  app.use(express.query());
  app.use(express.bodyParser({ keepExtensions: true }) );
  app.use(express.methodOverride());
	app.use(express.static(CONTENT_DIR));
	app.use(app.router);
	app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.all('/client/', function(req, res){
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("client ip is " + ip);
	res.json({'Result' : 'Ok'});
});

app.listen(31012);

// console.log('Hello my little world!');

var chokidar = require('chokidar');

var watcher = chokidar.watch(CONTENT_DIR, {ignored: /^\./, persistent: true});

watcher
  .on('add', function(path) {
  	// console.log('File', path, 'has been added');
  })
  .on('change', function(path) {

  	var short_filename = path.replace(CONTENT_DIR, '');

  	console.log('SERVER: File', short_filename, 'has been changed');


	var options = {
	  host: '127.0.0.1',
	  port: 31999,
	  path: '/?file=' + short_filename
	};

  	http.get(options, function(){
  		//...
  	});
  })
  .on('unlink', function(path) {console.log('File', path, 'has been removed');})
  .on('error', function(error) {console.error('Error happened', error);})

// 'add' and 'change' events also receive stat() results as second argument.
// http://nodejs.org/api/fs.html#fs_class_fs_stats
// watcher.on('change', function(path, stats) {
//   // console.log('File', path, 'changed size to', stats.size);
// });

// watcher.add('new-file');
// watcher.add(['new-file-2', 'new-file-3']);

// Only needed if watching is persistent.
watcher.close();

var app_test_client = express();

app_test_client.configure(function(){
	// app_test_client.use(express.logger('dev'));
    app_test_client.use(express.query());
    app_test_client.use(express.bodyParser({ keepExtensions: true }) );
    app_test_client.use(express.methodOverride());
	app_test_client.use(app_test_client.router);
	app_test_client.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app_test_client.listen(31999);

app_test_client.all('/', function(req, res){
	// var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("file changed: " + req.query.file);
	res.json({'Result' : 'Ok'});
});